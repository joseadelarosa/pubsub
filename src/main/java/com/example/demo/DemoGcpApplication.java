package com.example.demo;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.gcp.pubsub.core.PubSubTemplate;
import org.springframework.context.annotation.Bean;


import lombok.extern.slf4j.Slf4j;

@SpringBootApplication
@Slf4j
public class DemoGcpApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoGcpApplication.class, args);
	}
	
  
	@SuppressWarnings("deprecation")
  @Bean
  public CommandLineRunner cli(PubSubTemplate pubSubTemplate) {
      return (args) -> {
          pubSubTemplate.subscribe("sub-apisrumbo-crerestprog-dev",
             (msg, ackConsumer) -> {
                  log.info(msg.getData().toStringUtf8());
                  ackConsumer.ack();
             });
      };
  }
	



}
