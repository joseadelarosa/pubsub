package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gcp.pubsub.core.PubSubTemplate;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.ImmutableMap;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("${server.servlet.context-path}")
@Slf4j
public class DemoGCPController {

  @Autowired
  private PubSubTemplate pubSubTemplate;

  @GetMapping(value = "/publishMessage", produces = { MediaType.APPLICATION_STREAM_JSON_VALUE,
      MediaType.APPLICATION_JSON_VALUE })
  public Mono<String> publishMessage(@RequestParam("message") String message) {
    pubSubTemplate.publish("tcp-apisrumbo-crerestprog-dev", "your message payload "+message, ImmutableMap.of("key1", "val1"));
    log.info("Se envió el mensaje: {} ", message);
    return Mono.just("El mensaje: "+message+", se envió correctamente.");
  }
  
  @GetMapping(value = "", produces = { MediaType.APPLICATION_STREAM_JSON_VALUE,
      MediaType.APPLICATION_JSON_VALUE })
  public Mono<String> dummy() {
    log.info("mensaje de prueba 1 Logback ");
    log.error("mensaje de error 2 Logback ");
    return Mono.just("Prueba de mensaje.");
  }

}
